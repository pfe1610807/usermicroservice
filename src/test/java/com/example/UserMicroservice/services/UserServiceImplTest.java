package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.*;
import com.example.UserMicroservice.repositories.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
public class UserServiceImplTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PortalRepository portalRepository;
    @Autowired
    private PortalInstanceRepository portalInstanceRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptEncoder;

    // Static variables for Portal
    private static final UUID anyPortalId = UUID.randomUUID();
    private static final String anyPortalName = "Any portal instance name";
    private static final String anyPortalDescription = "Any portal instance description";
    private static final Boolean anyPortalIsDeleted = false;
    private static final List<Permission> anyPortalPermissions = new ArrayList<>();
    private static final List<PortalInstance> anyPortalInstances = new ArrayList<>();
    private static final Portal anyPortal = new Portal(anyPortalId, anyPortalName, anyPortalDescription, anyPortalPermissions, anyPortalIsDeleted, anyPortalInstances);

    // Static variables for Portal Instance
    private static final UUID anyPortalInstanceId = UUID.randomUUID();
    private static final String anyPortalInstanceName = "Any portal instance name";
    private static final String anyPortalInstanceDescription = "Any portal instance description";
    private static final List<Role> anyPortalRoles = new ArrayList<>();
    private static final PortalInstance anyPortalInstance = new PortalInstance(anyPortalInstanceId, anyPortalInstanceName, anyPortalInstanceDescription, anyPortal, false, anyPortalRoles);


    // Static variables for Role
    private static final UUID anyRoleId = UUID.randomUUID();
    private static final String roleName = "ROLE_USER";
    private static final List<Permission> anyPermissionsList = new ArrayList<>();
    private static final List<User> anyUserList = new ArrayList<>();
    private static final Role anyRole = new Role(anyRoleId, roleName, false, null, anyUserList, anyPermissionsList);


    // Static variables for Users
    private static final UUID anyUserId = UUID.randomUUID();
    private static final String username = "user1";
    private static final String password = "user1";
    private static final String firstName = "user1";
    private static final String lastName = "user1";
    private static final List<Role> anyRoles = new ArrayList<>();

    static {
        anyRoles.add(anyRole);
    }

    private static final User anyUser = new User(anyUserId, username, password, firstName, lastName, true, false, 1, anyRoles, new ArrayList<>());

    @AfterEach
    void afterEach() {
        userRepository.deleteAll();
        roleRepository.deleteAll();
        portalInstanceRepository.deleteAll();
        portalRepository.deleteAll();
    }

    @BeforeEach
    void beforeEach() {
        portalRepository.saveAndFlush(anyPortal);
        portalInstanceRepository.saveAndFlush(anyPortalInstance);
        roleRepository.saveAndFlush(anyRole);
        userRepository.saveAndFlush(anyUser);
    }

    @Test
    void saveUser_success() {
        // Encode the password
        String encodedPassword = bCryptEncoder.encode(anyUser.getPassword());
        anyUser.setPassword(encodedPassword);

        UUID savedUserId = userService.saveUser(anyUser);
        assertNotNull(savedUserId);
        Optional<User> savedUserOptional = userRepository.findById(savedUserId);
        assertTrue(savedUserOptional.isPresent());
        User savedUser = savedUserOptional.get();
        assertEquals(anyUser.getUsername(), savedUser.getUsername());
        assertNotEquals(password, savedUser.getPassword());
//        assertTrue(bCryptEncoder.matches(password, bCryptEncoder.encode(savedUser.getPassword())));

    }

    @Test
    void findByUsername_existingUser_returnUser() {
        Optional<User> optionalUser = userService.findByUsername(anyUser.getUsername());
        assertTrue(optionalUser.isPresent());
        User retrievedUser = optionalUser.get();
        assertEquals(anyUser.getUserId(), retrievedUser.getUserId());
        assertEquals(anyUser.getUsername(), retrievedUser.getUsername());
        assertEquals(anyUser.getFirstName(), retrievedUser.getFirstName());
        assertEquals(anyUser.getLastName(), retrievedUser.getLastName());
        assertEquals(anyUser.getPassword(), retrievedUser.getPassword());
    }

    @Test
    void findByUsername_nonExistingUser_returnEmpty() {
        Optional<User> optionalUser = userService.findByUsername("nonExistingUsername");
        assertFalse(optionalUser.isPresent());
    }

    @Test
    void loadUserByUsername_existingUser_returnUserDetails() {
        UserDetails userDetails = userService.loadUserByUsername(anyUser.getUsername());
        assertEquals(anyUser.getUsername(), userDetails.getUsername());
        assertEquals(anyUser.getPassword(), userDetails.getPassword());
    }

    @Test
    void getUserById_existingUser_returnUser() {
        Optional<User> optionalUser = userService.getUserById(anyUser.getUserId());
        assertTrue(optionalUser.isPresent());
        User retrievedUser = optionalUser.get();
        assertEquals(anyUser.getUserId(), retrievedUser.getUserId());
        assertEquals(anyUser.getUsername(), retrievedUser.getUsername());
        assertEquals(anyUser.getFirstName(), retrievedUser.getFirstName());
        assertEquals(anyUser.getLastName(), retrievedUser.getLastName());
        assertEquals(anyUser.getPassword(), retrievedUser.getPassword());
    }

    @Test
    void getUserById_nonExistingUser_returnEmpty() {
        Optional<User> optionalUser = userService.getUserById(UUID.randomUUID());
        assertFalse(optionalUser.isPresent());
    }

    @Test
    void getUserFromDatabase_existingUser_returnUser() {
        User retrievedUser = userService.getUserFromDatabase(anyUser.getUsername());
        // Verify the user is retrieved
        assertNotNull(retrievedUser);
        assertEquals(anyUser.getUserId(), retrievedUser.getUserId());
        assertEquals(anyUser.getUsername(), retrievedUser.getUsername());
    }

    @Test
    void getUserFromDatabase_nonExistingUser_throwRuntimeException() {
        assertThrows(RuntimeException.class, () -> {
            userService.getUserFromDatabase("nonExistingUsername");
        });
    }
    @Test
    void updateUser_existingUser_returnUpdatedUser() {
        anyUser.setUsername("updatedUsername");
        anyUser.setFirstName("updatedFirstName");
        anyUser.setLastName("updatedLastName");
        anyUser.setPassword("updatedPassword");
        anyUser.setRoles(anyRoles);
        User returnedUser = userService.updateUser(anyUser.getUserId(), anyUser);
        assertNotNull(returnedUser);
        assertEquals("updatedUsername", returnedUser.getUsername());
        assertEquals("updatedFirstName", returnedUser.getFirstName());
        assertEquals("updatedLastName", returnedUser.getLastName());
        assertEquals("updatedPassword", returnedUser.getPassword());

    }
    @Test
    void softDeleteUser_existingUser_returnSoftDeletedUser() {
        User softDeletedUser = userService.softDeleteUser(anyUser.getUserId());

        assertNotNull(softDeletedUser);
        assertTrue(softDeletedUser.getIsDeleted());
        Optional<User> optionalUser = userRepository.findById(anyUser.getUserId());
        assertTrue(optionalUser.isPresent());
        assertTrue(optionalUser.get().getIsDeleted());
    }
    @Test
    void softDeleteUser_nonExistingUser_returnNull() {
        User softDeletedUser = userService.softDeleteUser(UUID.randomUUID());

        // Verify the returned value is null
        assertNull(softDeletedUser);
    }
    @Test
    void getUsers_onlyActiveUsers_returnActiveUsers() {
        List<User> users = userService.getUsers();
        assertEquals(1, users.size());
        assertEquals(anyUser.getUsername(), users.get(0).getUsername());
    }
    @Test
    void updateFirstLogin_existingUser_firstLoginUpdated() {
        UUID userId = anyUser.getUserId();
        anyUser.setFirstLogin(0);
        userService.updateFirstLogin(userId);

        Optional<User> updatedUser = userRepository.findById(userId);
        assertTrue(updatedUser.isPresent());
        assertEquals(0, updatedUser.get().getFirstLogin());
    }
}
