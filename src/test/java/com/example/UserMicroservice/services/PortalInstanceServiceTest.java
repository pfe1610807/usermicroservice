//package com.example.UserMicroservice.services;
//
//import com.example.UserMicroservice.entities.Permission;
//import com.example.UserMicroservice.entities.Portal;
//import com.example.UserMicroservice.entities.PortalInstance;
//import com.example.UserMicroservice.entities.Role;
//import com.example.UserMicroservice.repositories.PortalInstanceRepository;
//import com.example.UserMicroservice.repositories.PortalRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//import java.util.UUID;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@SpringBootTest
//@ActiveProfiles("test")
//public class PortalInstanceServiceTest {
//
//    @Autowired
//    private PortalInstanceRepository portalInstanceRepository;
//    @Autowired
//    private PortalRepository portalRepository;
//    @Autowired
//    private PortalInstanceService portalInstanceService;
//    // Static variables for Portal
//    private static final UUID anyPortalId = UUID.randomUUID();
//    private static final String anyPortalName = "Any portal instance name";
//    private static final String anyPortalDescription = "Any portal instance description";
//    private static final Boolean anyPortalIsDeleted = false;
//    private static final List<Permission> anyPortalPermissions = new ArrayList<>();
//    private static final List<PortalInstance> anyPortalInstances = new ArrayList<>();
//    private static final Portal anyPortal = new Portal(anyPortalId, anyPortalName, anyPortalDescription, anyPortalPermissions, anyPortalIsDeleted, anyPortalInstances);
//
//    // Static variables for Portal Instance
//    private static final UUID anyPortalInstanceId = UUID.randomUUID();
//    private static final String anyPortalInstanceName = "Any portal instance name";
//    private static final String anyPortalInstanceDescription = "Any portal instance description";
//    private static final List<Role> anyPortalRoles = new ArrayList<>();
//    private static final PortalInstance anyPortalInstance = new PortalInstance(anyPortalInstanceId, anyPortalInstanceName, anyPortalInstanceDescription, anyPortal, false, anyPortalRoles);
//
//
//    private Portal portal;
//
//    @BeforeEach
//    void beforeEach() {
//        portal = new Portal(UUID.randomUUID(), "Portal1", "Description1", new ArrayList<>(), false, new ArrayList<>());
//        portalRepository.save(portal);
//    }
//
//    @Test
//    void createPortalInstance_success() {
//        PortalInstance portalInstance = new PortalInstance(UUID.randomUUID(), "http://example.com", "Instance1", portal, false, new ArrayList<>());
//        PortalInstance savedPortalInstance = portalInstanceService.createPortalInstance(portalInstance);
//        assertNotNull(savedPortalInstance.getPortalInstanceId());
//        assertEquals(portalInstance.getInstanceName(), savedPortalInstance.getInstanceName());
//        assertEquals(portal, savedPortalInstance.getPortal());
//    }
//
//    @Test
//    void softDeletePortalInstance_success() {
//        PortalInstance portalInstance = new PortalInstance(UUID.randomUUID(), "http://example.com", "Instance1", portal, false, new ArrayList<>());
//        portalInstanceRepository.save(portalInstance);
//        PortalInstance deletedPortalInstance = portalInstanceService.softDeletePortalInstance(portalInstance.getPortalInstanceId());
//        assertTrue(deletedPortalInstance.getIsDeleted());
//    }
//
//    @Test
//    void updatePortalInstance_success() {
//        PortalInstance portalInstance = new PortalInstance(UUID.randomUUID(), "http://example.com", "Instance1", portal, false, new ArrayList<>());
//        portalInstanceRepository.save(portalInstance);
//        portalInstance.setInstanceName("Updated Instance");
//        PortalInstance updatedPortalInstance = portalInstanceService.updatePortalInstance(portalInstance.getPortalInstanceId(), portalInstance);
//        assertEquals("Updated Instance", updatedPortalInstance.getInstanceName());
//    }
//
//    @Test
//    void getAllPortalInstance_success() {
//        PortalInstance portalInstance1 = new PortalInstance(UUID.randomUUID(), "http://example.com", "Instance1", portal, false, new ArrayList<>());
//        PortalInstance portalInstance2 = new PortalInstance(UUID.randomUUID(), "http://example.com", "Instance2", portal, false, new ArrayList<>());
//        portalInstanceRepository.save(portalInstance1);
//        portalInstanceRepository.save(portalInstance2);
//        List<PortalInstance> portalInstances = portalInstanceService.getAllPortalInstance();
//        assertEquals(7, portalInstances.size());
//    }
//
//    @Test
//    void getNotDeletedPortalInstance_success() {
//        PortalInstance portalInstance1 = new PortalInstance(UUID.randomUUID(), "http://example.com", "Instance1", portal, false, new ArrayList<>());
//        PortalInstance portalInstance2 = new PortalInstance(UUID.randomUUID(), "http://example.com", "Instance2", portal, true, new ArrayList<>());
//        portalInstanceRepository.save(portalInstance1);
//        portalInstanceRepository.save(portalInstance2);
//        List<PortalInstance> portalInstances = portalInstanceService.getNotDeletedPortalInstance();
//        assertEquals(7, portalInstances.size());
//    }
//}
package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.Permission;
import com.example.UserMicroservice.entities.Portal;
import com.example.UserMicroservice.entities.PortalInstance;
import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.repositories.PortalInstanceRepository;
import com.example.UserMicroservice.repositories.PortalRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
public class PortalInstanceServiceTest {

    @Autowired
    private PortalInstanceRepository portalInstanceRepository;
    @Autowired
    private PortalRepository portalRepository;
    @Autowired
    private PortalInstanceService portalInstanceService;
    // Static variables for Portal
    private static final UUID anyPortalId = UUID.randomUUID();
    private static final String anyPortalName = "Any portal instance name";
    private static final String anyPortalDescription = "Any portal instance description";
    private static final Boolean anyPortalIsDeleted = false;
    private static final List<Permission> anyPortalPermissions = new ArrayList<>();
    private static final List<PortalInstance> anyPortalInstances = new ArrayList<>();
    private static final Portal anyPortal = new Portal(anyPortalId, anyPortalName, anyPortalDescription, anyPortalPermissions, anyPortalIsDeleted, anyPortalInstances);

    // Static variables for Portal Instance
    private static final UUID anyPortalInstanceId = UUID.randomUUID();
    private static final String anyPortalInstanceName = "Any portal instance name";
    private static final String anyPortalInstanceDescription = "Any portal instance description";
    private static final List<Role> anyPortalRoles = new ArrayList<>();
    private static final PortalInstance anyPortalInstance = new PortalInstance(anyPortalInstanceId, anyPortalInstanceName, anyPortalInstanceDescription, anyPortal, false, anyPortalRoles);


    @AfterEach
    void afterEach() {

        portalInstanceRepository.deleteAll();
        portalRepository.deleteAll();
    }

    @BeforeEach
    void beforeEach() {
        portalRepository.saveAndFlush(anyPortal);
        portalInstanceRepository.saveAndFlush(anyPortalInstance);
    }
    @Test
    void createPortalInstance_success() {
        PortalInstance portalInstance = new PortalInstance();
        portalInstance.setPortal(anyPortal);
        // Call the method to create the PortalInstance
        PortalInstance createdPortalInstance = portalInstanceService.createPortalInstance(portalInstance);

        // Verify the PortalInstance was created with the correct Portal and a new UUID
        assertNotNull(createdPortalInstance);
        assertNotNull(createdPortalInstance.getPortalInstanceId());
        assertEquals(anyPortal.getPortalId(), createdPortalInstance.getPortal().getPortalId());

    }

    @Test
    void softDeletePortalInstance_success() {
        PortalInstance deletedPortalInstance = portalInstanceService.softDeletePortalInstance(anyPortalInstance.getPortalInstanceId());
        assertNotNull(deletedPortalInstance);
        assertTrue(deletedPortalInstance.getIsDeleted());
        Optional<PortalInstance> optionalPortalInstance = portalInstanceRepository.findById(anyPortalInstance.getPortalInstanceId());
        assertTrue(optionalPortalInstance.isPresent());
        assertTrue(optionalPortalInstance.get().getIsDeleted());
    }

    @Test
    void updatePortalInstance_success() {
        portalInstanceRepository.save(anyPortalInstance);
        anyPortalInstance.setInstanceName("Updated Instance");
        PortalInstance updatedPortalInstance = portalInstanceService.updatePortalInstance(anyPortalInstance.getPortalInstanceId(), anyPortalInstance);
        assertEquals("Updated Instance", updatedPortalInstance.getInstanceName());
    }

    @Test
    void getAllPortalInstance_success() {
        List<PortalInstance> portalInstances = portalInstanceService.getAllPortalInstance();
        assertEquals(1, portalInstances.size());
    }

    @Test
    void getNotDeletedPortalInstance_success() {
        List<PortalInstance> portalInstances = portalInstanceService.getNotDeletedPortalInstance();
        assertEquals(1, portalInstances.size());
    }
}
