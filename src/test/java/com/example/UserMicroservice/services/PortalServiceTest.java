package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.Permission;
import com.example.UserMicroservice.entities.Portal;
import com.example.UserMicroservice.entities.PortalInstance;
import com.example.UserMicroservice.repositories.PortalRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
public class PortalServiceTest {
    // Portal
    private static final UUID anyPortalId = UUID.randomUUID();
    private static final String anyPortalName = "Any portal instance name";
    private static final String anyPortalDescription = "Any portal instance description";
    private static final Boolean anyPortalIsDeleted = false;
    private static final List<Permission> anyPortalPermissions = new ArrayList<>();
    private static final List<PortalInstance> anyPortalInstances = new ArrayList<>();
    private static final Portal anyPortal = new Portal(anyPortalId, anyPortalName, anyPortalDescription,anyPortalPermissions, anyPortalIsDeleted, anyPortalInstances);


    @Autowired
    private PortalService portalService;

    @Autowired
    private PortalRepository portalRepository;

    @AfterEach
    void afterEach() {
        portalRepository.deleteAll();
    }

    @BeforeEach
    void beforeEach() {
        portalRepository.saveAndFlush(anyPortal);
    }

    @Test
    public void createPortal_whenValidData_shouldCreatePortal() {
        Portal result = portalService.createPortal(anyPortal);

        assertNotNull(result.getPortalId());
        assertEquals(anyPortalName, result.getPortalName());

        Portal savedPortal = portalRepository.findById(result.getPortalId()).orElse(null);
        assertNotNull(savedPortal);
        assertEquals(anyPortalName, savedPortal.getPortalName());
    }

//    @Test
//    public void getNotDeleted_whenCalled_shouldReturnNotDeletedPortals() {
//        List<Portal> result = portalService.getNotDeleted();
//        assertEquals(1, result.size());
//        assertEquals(anyPortalName, result.get(0).getPortalName());
//        assertEquals(anyPortalName, result.get(1).getPortalName());
//    }
}
