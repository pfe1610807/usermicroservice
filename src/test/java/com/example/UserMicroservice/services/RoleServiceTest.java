package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.*;
import com.example.UserMicroservice.repositories.PortalInstanceRepository;
import com.example.UserMicroservice.repositories.PortalRepository;
import com.example.UserMicroservice.repositories.RoleRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class RoleServiceTest {
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PortalRepository portalRepository;
    @Autowired
    PortalInstanceRepository portalInstanceRepository;
    @Autowired
    RoleService roleService;

    // Portal
    private static final UUID anyPortalId = UUID.randomUUID();
    private static final String anyPortalName = "Any portal instance name";
    private static final String anyPortalDescription = "Any portal instance description";
    private static final Boolean anyPortalIsDeleted = false;
    private static final List<Permission> anyPortalPermissions = new ArrayList<>();
    private static final List<PortalInstance> anyPortalInstances = new ArrayList<>();
    private static final Portal anyPortal = new Portal(anyPortalId, anyPortalName, anyPortalDescription,anyPortalPermissions, anyPortalIsDeleted, anyPortalInstances);


    // Portal Instance
    private static final UUID anyPortalInstanceId = UUID.randomUUID();
    private static final String anyPortalInstanceName = "Any portal instance name";
    private static final String anyPortalInstanceDescription = "Any portal instance description";
    private static final List<Role> anyPortalRoles = new ArrayList<>();

    private static final PortalInstance anyPortalInstance = new PortalInstance(anyPortalInstanceId, anyPortalInstanceName, anyPortalInstanceDescription, anyPortal, false,anyPortalRoles);


    // Role
    private static final UUID anyRoleId = UUID.randomUUID();
    private static final String anyRoleName = "Any role name";
    private static final Boolean anyRoleIsDeleted = false;
    private static final List<User> anyRoleUsers = new ArrayList<>();
    private static final List<Permission> anyRolePermissions = new ArrayList<>();


    private static final Role anyRole = new Role(
            anyRoleId,
            anyRoleName,
            anyRoleIsDeleted,
            anyPortalInstance,
            anyRoleUsers,
            anyRolePermissions
    );
    private static final Role anySoftDeletedRole = new Role(
            anyRoleId,
            anyRoleName,
            true,
            anyPortalInstance,
            anyRoleUsers,
            anyRolePermissions
    );

    @BeforeEach
    void beforeEach() {
        portalRepository.saveAndFlush(anyPortal);
        portalInstanceRepository.saveAndFlush(anyPortalInstance);
        roleRepository.saveAndFlush(anyRole);
    }

    @AfterEach
    void afterEach() {
         roleRepository.deleteAll();
         portalInstanceRepository.deleteAll();
         portalRepository.deleteAll();
    }

    // create role with valid data
    @Test
    void createRole_whenValidData_resultingCreateRole() {
        // Act
        Role savedRole = roleService.createRole(anyRole);

        // Assert
        assertNotNull(savedRole);
        assertEquals(anyRole.getRoleId(), savedRole.getRoleId());
        assertEquals(anyRole.getRoleName(), savedRole.getRoleName());
        assertEquals(anyRole.getIsDeleted(), savedRole.getIsDeleted());
        assertEquals(anyRole.getPortalInstance().getPortalInstanceId(), savedRole.getPortalInstance().getPortalInstanceId());
        assertEquals(anyRole.getUsers(), savedRole.getUsers());
        assertEquals(anyRole.getPermissions(), savedRole.getPermissions());
    }
//    @Test
//    void getRole_withRoleId_returningRole() {
//        // Arrange
//        Role actualRole = roleRepository.findById(anyRoleId).orElseThrow();
//
//        // Act
//        Role role = roleService.getRoleById(anyRoleId);
//
//        // Assert
//        assertNotNull(role);
//        assertEquals(role.getRoleId(), actualRole.getRoleId());
//        assertEquals(role.getRoleName(), actualRole.getRoleName());
//        assertEquals(anyRole.getIsDeleted(), actualRole.getIsDeleted());
//        assertEquals(anyRole.getPortalInstance().getPortalInstanceId(), actualRole.getPortalInstance().getPortalInstanceId());
////        assertEquals(anyRole.getUsers(), actualRole.getUsers());
////        assertEquals(anyRole.getPermissions(), actualRole.getPermissions());
//    }

//    @Test
//    void getRoles_onlyNonDeletedRoles_returningNonDeletedRoles() {
//        // Arrange
//        roleRepository.saveAndFlush(anySoftDeletedRole);
//
//        // Act
//        List<Role> nonDeletedRoles = roleService.getNotDeletedRoles();
//        System.out.println(roleService.getAllRoles().size());
//        // Assert
//        assertEquals(1, nonDeletedRoles.size());
//        assertFalse(nonDeletedRoles.get(0).getIsDeleted());
//    }
    @Test
    void deleteRole_withRoleId_resultingIsDeletedTrue() {
        // Act
        roleService.softDeleteRole(anyRole.getRoleId());

        // Assert
        Role deletedRole = roleRepository.findById(anyRole.getRoleId()).orElse(null);
        assertNotNull(deletedRole);
        assertTrue(deletedRole.getIsDeleted());
    }

    @Test
    void updateRole_withExistingRoleId_returningUpdatedRole() {
        // Arrange
        anyRole.setRoleName("Role name 2");

        // Act
        Role updatedRole = roleService.updateRole(anyRoleId, anyRole);

        // Assert
        assertEquals(updatedRole.getRoleName(), "Role name 2");
    }


}
