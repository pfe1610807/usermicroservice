package com.example.UserMicroservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "portals")
public class Portal {
    @Id
    private UUID portalId;
    @Column(name = "portal_name")
    private String portalName;
    @Column(name = "portal_descriptions")
    private String portalDescription;
    @OneToMany(mappedBy = "portal", cascade = CascadeType.ALL)
    private List<Permission> permissions = new ArrayList<>();
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;
    @OneToMany(mappedBy = "portal", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<PortalInstance> portalInstance = new ArrayList<>();
}
