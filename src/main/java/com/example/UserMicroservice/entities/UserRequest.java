package com.example.UserMicroservice.entities;

import lombok.Data;

@Data
public class UserRequest {

    private String username;
    private String password;
}
