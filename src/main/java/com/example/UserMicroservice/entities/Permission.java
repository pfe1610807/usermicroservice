package com.example.UserMicroservice.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import java.util.List;
import java.util.UUID;
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "permissions")
public class Permission {
    @Id
    private UUID permissionId;
    @Column(name = "permission_name")
    private String permissionName;
    @ManyToMany(mappedBy = "permissions")
    @JsonIgnore
    private List<Role> roles;
    @ManyToOne
    @JsonIgnore
    private Portal portal;
}
