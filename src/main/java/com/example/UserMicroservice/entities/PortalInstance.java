package com.example.UserMicroservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PortalInstances")
public class PortalInstance {
    @Id
    private UUID PortalInstanceId;
    @Column(name = "PortalInstance_url")
    private String instanceUrl;
    @Column(name = "instance_Name")
    private String instanceName;
    @ManyToOne
    private Portal portal;
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

    @OneToMany(mappedBy = "portalInstance")
    @JsonIgnore
    private List<Role> roles = new ArrayList<>();
}
