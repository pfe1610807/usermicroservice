package com.example.UserMicroservice.controllers;

import com.example.UserMicroservice.entities.User;
import com.example.UserMicroservice.entities.UserRequest;
import com.example.UserMicroservice.entities.UserResponse;
import com.example.UserMicroservice.jwt.JWTUtil;
import com.example.UserMicroservice.repositories.UserRepository;
import com.example.UserMicroservice.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JWTUtil util;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/sign-up")
    public ResponseEntity<String> saveTheUser(@RequestBody User user) {
        UUID id = userService.saveUser(user);
        if (id != null) {
            return ResponseEntity.ok("User with id '" + id + "' saved succssfully!");
        } else {
            return ResponseEntity.badRequest().body("Failed to save user.");
        }
    }

    @GetMapping
    public List<User> getUsers() {
        return userService.getUsers();

    }

    @PostMapping("/login")
    public ResponseEntity<UserResponse> login(@RequestBody UserRequest request) {
        //auth and sessions  Spring Security framework.
        //pour gere les session les connection dune facon sync donc sec toutal
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        if (authentication.isAuthenticated()) {
            User user = userService.getUserFromDatabase(request.getUsername());
            UserResponse response;
            String token = util.generateToken(request.getUsername(), user);
            response = new UserResponse(token, "Token generated successfully!");
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new UserResponse(null, "Authentication failed"));
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity<User> UpdateUser(@PathVariable UUID id, @RequestBody User updatedUser) {
        User updated = userService.updateUser(id, updatedUser);
        return ResponseEntity.ok(updated);
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<User> softDeleteUser(@PathVariable UUID id) {
        User softDeletedUser = userService.softDeleteUser(id);
        if (softDeletedUser == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(softDeletedUser);
    }

    @GetMapping("/permissions/{userId}")
    public ResponseEntity<Map<String, List<String>>> getPermissionsByUserId(@PathVariable UUID userId) {
        List<String> permissions = userService.getPermissionsByUserId(userId);
        Map<String, List<String>> permissionMap = new HashMap<>();
        permissionMap.put("permissionsName", permissions);
        return ResponseEntity.ok(permissionMap);
    }


    @GetMapping("/{userId}")
    public Optional<User> getUserById(@PathVariable UUID userId) {
        return userService.getUserById(userId);
    }

    @PatchMapping("/{userId}")
    public ResponseEntity<User> updateFirstLogin(@PathVariable UUID userId) {
        userService.updateFirstLogin(userId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/validate")
    public ResponseEntity<Boolean> validateToken(
            @RequestParam String token,
            @RequestParam String username) {
        System.out.println("Received Token: " + token);
        boolean isValid = util.isValidToken(token, username);
        return ResponseEntity.ok(isValid);
    }
}

