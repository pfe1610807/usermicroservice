package com.example.UserMicroservice.controllers;

import com.example.UserMicroservice.entities.Portal;
import com.example.UserMicroservice.entities.PortalInstance;
import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.repositories.PortalInstanceRepository;
import com.example.UserMicroservice.repositories.PortalRepository;
import com.example.UserMicroservice.services.PortalInstanceService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/portalInstances")
public class PortalInstanceController {
    @Autowired
    private PortalInstanceService portalInstanceService;
    @Autowired
    private PortalInstanceRepository portalInstanceRepository;
    @Autowired
    private PortalRepository portalRepository;

//    @GetMapping()
//    public List<PortalInstance> getAllPortalInstances() {
//        return portalInstanceService.getAllPortalInstance();
//    }
    @GetMapping()
    public List<PortalInstance> getNotDeletedPortalInstances() {
        return portalInstanceService.getNotDeletedPortalInstance();
    }
    @PostMapping
    public ResponseEntity<PortalInstance> createPortalInstance(@RequestBody PortalInstance portalInstance) {
        PortalInstance createdPortalInstance = portalInstanceService.createPortalInstance(portalInstance);
        return new ResponseEntity<>(createdPortalInstance, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PortalInstance> updatePortalInstance(@PathVariable UUID id, @RequestBody PortalInstance portalInstance) {
        PortalInstance updatedPortalInstance = portalInstanceService.updatePortalInstance(id, portalInstance);
        if (updatedPortalInstance != null) {
            return new ResponseEntity<>(updatedPortalInstance, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{portalInstance-id}")
    public ResponseEntity<PortalInstance> softDeletePortalInstance(@PathVariable("portalInstance-id") UUID roleId) {
        PortalInstance softDeletePortalInstance = portalInstanceService.softDeletePortalInstance(roleId);
        return ResponseEntity.ok(softDeletePortalInstance);
    }
    @GetMapping("/portals/{id}")
    public ResponseEntity<List<PortalInstance>> getPortalInstances(@PathVariable UUID id) {
        Portal portal = portalRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Portal not found with ID: " + id));
        List<PortalInstance> instances = portalInstanceRepository.findByPortal(portal);
        return ResponseEntity.ok(instances);
    }
}
