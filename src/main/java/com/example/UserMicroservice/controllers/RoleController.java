package com.example.UserMicroservice.controllers;

import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
@RestController
@RequestMapping("/roles")
public class RoleController {
    @Autowired
    private final RoleService roleService;
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }
    @GetMapping()
    public List<Role> getAllRoles() {
        return roleService.getNotDeletedRoles();
    }
    @GetMapping("/{role-id}")
    public Role getRoleById(@PathVariable("role-id") UUID roleId) {
        return roleService.getRoleById(roleId);
    }
    @PostMapping
    public ResponseEntity<Role> createRole(@RequestBody Role role){
        Role savedRole = roleService.createRole(role);
        return ResponseEntity.ok().body(savedRole);
    }
    @PatchMapping("/{role-id}")
    public ResponseEntity<Role> updateRole(@PathVariable("role-id") UUID roleId, @RequestBody Role role) {
        return ResponseEntity.ok(roleService.updateRole(roleId,role));
    }
    @DeleteMapping("/{role-id}")
    public ResponseEntity<Role> softDeleteRole(@PathVariable("role-id") UUID roleId) {
        Role softDeletedRole = roleService.softDeleteRole(roleId);
        return ResponseEntity.ok(softDeletedRole);
    }

}
