package com.example.UserMicroservice.controllers;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.example.UserMicroservice.entities.Notification;
import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.services.NotificationService;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Component
@RestController
@RequestMapping("/notifications")
public class NotificationController {
    @Autowired
    private SocketIOServer socketIOServer;
    @Autowired
    private NotificationService notificationService;

    @PostConstruct
    public void init() {
        socketIOServer.addConnectListener(new ConnectListener() {
            @Override
            public void onConnect(SocketIOClient client) {
                System.out.println("Client connected: " + client.getSessionId());
            }
        });

        socketIOServer.addDisconnectListener(new DisconnectListener() {
            @Override
            public void onDisconnect(SocketIOClient client) {
                System.out.println("Client disconnected: " + client.getSessionId());
            }
        });

        socketIOServer.addEventListener("sendNotification", UserIdMessage.class, new DataListener<UserIdMessage>() {
            @Override
            public void onData(SocketIOClient client, UserIdMessage userIdMessage, AckRequest ackRequest) {
                System.out.println("Received message from client: " + userIdMessage.getUserId());
                notificationService.createNotification("A new record template is published.", userIdMessage.getUserId(),userIdMessage.getEntity(),userIdMessage.getEntityId());
                for (SocketIOClient socketIOClient : socketIOServer.getAllClients()) {
                    if (!socketIOClient.getSessionId().equals(client.getSessionId())) {
                        socketIOClient.sendEvent("notification", "Hello from Spring");
                    }
                }

            }
        });
    }

    @PreDestroy
    public void stopServer() {
        socketIOServer.stop();
    }


    @GetMapping("/{user-id}")
    public List<Notification> getNotificationsByUser(@PathVariable("user-id") UUID userId) {
        return notificationService.getNotificationsByUser(userId);
    }
    @PutMapping("/{notificationId}")
    public ResponseEntity<Notification> markNotificationAsRead(@PathVariable UUID notificationId) {
        notificationService.markNotificationAsRead(notificationId);
        return ResponseEntity.ok().build();
    }
    @PatchMapping("/user/{user-id}")
    public ResponseEntity<Notification> markNotificationsAsRead(@PathVariable("user-id") UUID userId) {
        notificationService.markALLNotificationsAsRead(userId);
        return ResponseEntity.ok().build();
    }
}

class UserIdMessage {
    private UUID userId;
    private  String entity;
    private UUID entityId;
    // Constructors, getters, and setters
    public UserIdMessage() {
    }

    public UserIdMessage(UUID userId) {
        this.userId = userId;
    }

    public UUID getUserId() {
        return userId;
    }
public  UUID getEntityId(){return  entityId;}
    public  String getEntity(){return  entity;}
    public void setUserId(UUID userId) {
        this.userId = userId;
    }
}
