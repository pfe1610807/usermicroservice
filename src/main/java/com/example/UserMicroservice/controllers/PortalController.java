package com.example.UserMicroservice.controllers;

import com.example.UserMicroservice.entities.Portal;
import com.example.UserMicroservice.services.PortalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/portals")
public class PortalController {
    @Autowired
    private final PortalService portalService;
    public PortalController(PortalService portalService) {
        this.portalService = portalService;
    }

    @GetMapping()
    public List<Portal> getNotDeleted() {
        return portalService.getNotDeleted();
    }
    @PostMapping
    public ResponseEntity<Portal> createPortal(@RequestBody Portal portal){
        Portal savedPortal = portalService.createPortal(portal);
        return ResponseEntity.ok().body(savedPortal);
    }


}
