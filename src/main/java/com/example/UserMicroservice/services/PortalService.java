package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.*;
import com.example.UserMicroservice.repositories.PermissionRepository;
import com.example.UserMicroservice.repositories.PortalInstanceRepository;
import com.example.UserMicroservice.repositories.PortalRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Service
public class PortalService {
     @Autowired
    private final PortalRepository portalRepository;

    public PortalService(PortalRepository portalRepository) {
        this.portalRepository = portalRepository ;
    }
    public Portal createPortal(Portal portal) {
        portal.setPortalId(UUID.randomUUID());
        return portalRepository.save(portal);
    }


    public List<Portal> getNotDeleted() {
        return portalRepository.findByIsDeletedFalse();
    }
}
