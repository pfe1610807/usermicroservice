package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.repositories.RoleRepository;
import com.example.UserMicroservice.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class RoleService {
    @Autowired
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }
    public List<Role> getNotDeletedRoles() {
        return  roleRepository.findByIsDeletedFalse();
    }

    public Role getRoleById(UUID id) {
        return roleRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    public Role createRole(Role role) {
        role.setRoleId(UUID.randomUUID());
        return roleRepository.save(role);
    }

    public Role updateRole(UUID id, Role updatedRole) {
        Role currentRole = roleRepository.findById(id).orElseThrow(RuntimeException::new);
        if (updatedRole.getRoleName() != null) {
            currentRole.setRoleName(updatedRole.getRoleName());
        }
        if (updatedRole.getPortalInstance() != null) {
            currentRole.setPortalInstance(updatedRole.getPortalInstance());
        }
        if (updatedRole.getPermissions() != null) {
            currentRole.setPermissions(updatedRole.getPermissions());
        }
        if (updatedRole.getUsers() != null) {
            currentRole.setUsers(updatedRole.getUsers());
        }
        return roleRepository.save(currentRole);
    }

    public Role softDeleteRole(UUID id) {
        Role role = roleRepository.findById(id).orElseThrow();
        role.setIsDeleted(true);
        return roleRepository.save(role);
    }
}
