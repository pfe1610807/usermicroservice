package com.example.UserMicroservice.services;

import com.example.UserMicroservice.Auditable;
import com.example.UserMicroservice.entities.Notification;
import com.example.UserMicroservice.entities.Portal;
import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.entities.User;
import com.example.UserMicroservice.repositories.NotificationRepository;
import com.example.UserMicroservice.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
@Service
public class NotificationService extends Auditable {
    @Autowired
    private final NotificationRepository notificationRepository;

    @Autowired
    private final UserRepository userRepository;
    public NotificationService(NotificationRepository notificationRepository, UserRepository userRepository) {
        this.notificationRepository = notificationRepository;
        this.userRepository = userRepository;

    }
    public List<Notification> getNotificationsByUser(UUID userId) {
        User user = userRepository.findById(userId).orElseThrow(RuntimeException::new);
        return notificationRepository.findByUser(user);
    }

    public void createNotification(String message, UUID userId,String entity,UUID entityId) {
        List<UUID> userIdsToExclude = Collections.singletonList(userId);
        List<User> allUsersExceptUser = userRepository.findAllByUserIdNotIn(userIdsToExclude);

        for (User user : allUsersExceptUser) {
            Notification newNotification = new Notification();
            newNotification.setId(UUID.randomUUID());
            newNotification.setIsRead(false);
            newNotification.setEntity(entity);
            newNotification.setEntityId(entityId);
            newNotification.setMessage(message);
            newNotification.setUser(user);
            notificationRepository.save(newNotification);
        }
    }
    public void markNotificationAsRead(UUID notificationId) {
        Notification notification = notificationRepository.findById(notificationId).orElseThrow();
        notification.setIsRead(true);
        notificationRepository.save(notification);
    }
 public void markALLNotificationsAsRead(UUID userId){
     List<Notification> notifications = notificationRepository.findByUserUserId(userId);
     for (Notification notification : notifications) {
         notification.setIsRead(true);
     }
      notificationRepository.saveAll(notifications);


 }
}
