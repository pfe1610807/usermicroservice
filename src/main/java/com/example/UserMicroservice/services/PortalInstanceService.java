package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.Permission;
import com.example.UserMicroservice.entities.Portal;
import com.example.UserMicroservice.entities.PortalInstance;
import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.repositories.PortalInstanceRepository;
import com.example.UserMicroservice.repositories.PortalRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PortalInstanceService {
    @Autowired
    PortalInstanceRepository portalInstanceRepository;
    @Autowired
    PortalRepository portalRepository;
    public List<PortalInstance> getAllPortalInstance() {
        return portalInstanceRepository.findAll();
    }

    public List<PortalInstance> getNotDeletedPortalInstance() {
        return  portalInstanceRepository.findByIsDeletedFalse();
    }
    public PortalInstance createPortalInstance(PortalInstance portalInstance) {
        UUID portalId = portalInstance.getPortal().getPortalId();
        Portal portal = portalRepository.findById(portalId)
            .orElseThrow(() -> new EntityNotFoundException("Portal not found with id: " + portalId));

        portalInstance.setPortalInstanceId(UUID.randomUUID());
        portalInstance.setPortal(portal);

        return portalInstanceRepository.save(portalInstance);
    }

    public PortalInstance softDeletePortalInstance(UUID id) {
        PortalInstance portalInstance = portalInstanceRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Portal not found with ID: " + id));
        portalInstance.setIsDeleted(true);
        return portalInstanceRepository.save(portalInstance);
    }

    public PortalInstance updatePortalInstance(UUID id, PortalInstance portalInstance) {
        if (portalInstanceRepository.existsById(id)) {
            portalInstance.setPortalInstanceId(id);
            return portalInstanceRepository.save(portalInstance);
        }
        return null;
    }
}
