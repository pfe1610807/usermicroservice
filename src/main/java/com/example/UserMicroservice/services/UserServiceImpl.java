package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.Permission;
import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.entities.User;
import com.example.UserMicroservice.repositories.RoleRepository;
import com.example.UserMicroservice.repositories.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptEncoder;

    @Override
    public UUID saveUser(User user) {
        List<Role> userRoles = user.getRoles();
        List<Role> rolesFoundByName = new ArrayList<>();
        for (Role role : userRoles) {
            Optional<Role> optionalRole = roleRepository.findById(role.getRoleId());
            if (optionalRole.isPresent()) {
                rolesFoundByName.add(optionalRole.get());
            } else {
                return null;
            }
        }
        user.setRoles(rolesFoundByName);
        user.setUserId(UUID.randomUUID());
        user.setUsername(user.getUsername());
        user.setLastName(user.getLastName());
        user.setFirstName(user.getFirstName());
        user.setFirstLogin(1);
        user.setPassword(bCryptEncoder.encode(user.getPassword()));
        return userRepository.save(user).getUserId();
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsernameWithRoles(username)
            .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));


        List<GrantedAuthority> authorities = user.getRoles().stream()
            .map(role -> new SimpleGrantedAuthority(role.getRoleName()))
            .collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(
            user.getUsername(),
            user.getPassword(),
            authorities);
    }

    public Optional<User> getUserById(UUID id) {
        return userRepository.findById(id);
    }

    @Override
    public User softDeleteUser(UUID id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setIsDeleted(true);
            return userRepository.save(user);
        } else {
            return null;
        }
    }

    public User updateUser(UUID id, User updatedUser) {
        User existingUser = userRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("User not found with id: " + id));

        existingUser.setUsername(updatedUser.getUsername());
        existingUser.setFirstName(updatedUser.getFirstName());
        existingUser.setLastName(updatedUser.getLastName());
        existingUser.setPassword(updatedUser.getPassword());

        List<Role> userRoles = updatedUser.getRoles();
        List<Role> rolesFoundByName = new ArrayList<>();
        for (Role role : userRoles) {
            Optional<Role> optionalRole = roleRepository.findById(role.getRoleId());
            if (optionalRole.isPresent()) {
                rolesFoundByName.add(optionalRole.get());
            } else {
                return null;
            }
        }
        existingUser.setRoles(rolesFoundByName);
        return userRepository.save(existingUser);
    }


    public User getUserFromDatabase(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            throw new RuntimeException("User not found with username: " + username);
        }
    }

    public List<String> getPermissionsByUserId(UUID userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new RuntimeException("User not found");
        }

        List<String> permissionNames = new ArrayList<>();
        for (Role role : user.getRoles()) {
            for (Permission permission : role.getPermissions()) {
                permissionNames.add(permission.getPermissionName());
            }
        }
        return permissionNames;
    }


    public List<User> getUsers() {
        List<User> allUsers = userRepository.findAll();
        return allUsers.stream()
            .filter(user -> !user.getIsDeleted())
            .collect(Collectors.toList());
    }


    public void updateFirstLogin(UUID userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null) {
            user.setFirstLogin(0);
            userRepository.save(user);
        }
    }

}
