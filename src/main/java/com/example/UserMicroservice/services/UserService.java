package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.User;

import java.util.Optional;
import java.util.UUID;

public interface UserService {
    UUID saveUser(User user);
    Optional<User> findByUsername(String username);
     User softDeleteUser(UUID id);
     User updateUser(UUID id, User updatedUser);
}
