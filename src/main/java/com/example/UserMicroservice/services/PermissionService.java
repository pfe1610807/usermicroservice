package com.example.UserMicroservice.services;

import com.example.UserMicroservice.entities.*;
import com.example.UserMicroservice.repositories.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PermissionService {
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private PortalRepository PortalRepository;
    @Autowired
    private PortalInstanceRepository PortalInstanceRepository;
    @Autowired
    private PortalInstanceRepository portalInstanceRepository;

    public List<Permission> getAllPermissions() {
        return permissionRepository.findAll();
    }

    public Permission getPermissionById(UUID id) {
        return permissionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Permission not found with id: " + id));
    }

    @Transactional
    public void loadPermissionsFromJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            InputStream inputStream = new ClassPathResource("permissions.json").getInputStream();
            JsonNode rootNode = mapper.readTree(inputStream);
            // Load permissions
            Permission[] permissions = mapper.readValue(rootNode.get("permissions").toString(), Permission[].class);
            loadPermissions(permissions);

            if (rootNode.has("portals")) {
                loadPortals(mapper, rootNode);
            }
            // Load portals
            if (rootNode.has("PortalInstances")) {
                loadPortalInstances(mapper, rootNode);
            }

            // Load roles and associate with permissions
            if (rootNode.has("roles")) {
                Role[] roles = mapper.readValue(rootNode.get("roles").toString(), Role[].class);
                for (Role role : roles) {
                    Optional<Role> existingRole = roleRepository.findByRoleName(role.getRoleName());
                    if (existingRole.isEmpty()) {
                        createRole(role);
                    } else {
                        Role existing = existingRole.get();
                        for (Permission permission : role.getPermissions()) {
                            Optional<Permission> permissionOptional = permissionRepository.findByPermissionName(permission.getPermissionName());
                            if (permissionOptional.isPresent()) {
                                Permission existingPermission = permissionOptional.get();
                                // Check if permission is already associated with the role
                                boolean isAlreadyAssociated = existing.getPermissions().stream()
                                        .anyMatch(p -> p.getPermissionId().equals(existingPermission.getPermissionId()));
                                if (!isAlreadyAssociated) {
                                    existing.getPermissions().add(existingPermission);
                                    roleRepository.save(existing);
                                    assignPermissionToRole(permission.getPermissionName(), existing.getRoleId());
                                }
                            } else {
                                throw new RuntimeException("Permission not found: " + permission.getPermissionName());
                            }
                        }
                    }
                }
            }
            if (rootNode.has("users")) {
                User[] users = mapper.readValue(rootNode.get("users").toString(), User[].class);
                for (User user : users) {
                    Optional<User> existingUserOptional = userRepository.findByUsername(user.getUsername());
                    if (existingUserOptional.isPresent()) {
                    } else {
                        createUser(user);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void createRole(Role role) {
        Role newRole = new Role();
        newRole.setRoleId(UUID.randomUUID());
        newRole.setRoleName(role.getRoleName());

        List<Permission> rolePermissions = new ArrayList<>();
        for (Permission permission : role.getPermissions()) {
            Optional<Permission> permissionOptional = permissionRepository.findByPermissionName(permission.getPermissionName());
            if (permissionOptional.isPresent()) {
                rolePermissions.add(permissionOptional.get());
            } else {
                throw new RuntimeException("Permission not found: " + permission.getPermissionName());
            }
        }
        newRole.setPermissions(rolePermissions);
        // Fetch the portal instance by name
        Optional<PortalInstance> portalInstanceOptional = Optional.ofNullable(PortalInstanceRepository.findByInstanceName(role.getPortalInstance().getInstanceName()));
        if (portalInstanceOptional.isPresent()) {
            newRole.setPortalInstance(portalInstanceOptional.get());
        } else {
            throw new RuntimeException("Portal not found: " + role.getPortalInstance().getInstanceName());
        }
        roleRepository.save(newRole);
    }

    private void loadPermissions(Permission[] permissions) {
        List<String> existingPermissionNames = new ArrayList<>();
        permissionRepository.findAll().forEach(permission -> existingPermissionNames.add(permission.getPermissionName()));//récupère toutes les permissions du DB
        List<Permission> permissionsToAdd = new ArrayList<>();
        for (Permission permission : permissions) {
            if (!existingPermissionNames.contains(permission.getPermissionName())) {
                permission.setPermissionId(UUID.randomUUID());
                if (permission.getPortal() != null) {
                    Optional<Portal> portalOptional = PortalRepository.findById(permission.getPortal().getPortalId());
                    if (portalOptional.isPresent()) {
                        permission.setPortal(portalOptional.get()); // Set portal directly
                    } else {
                        throw new RuntimeException("Portal not found for id: " + permission.getPortal().getPortalId());
                    }
                }
                permissionsToAdd.add(permission);
                existingPermissionNames.add(permission.getPermissionName());
            }
        }
        permissionRepository.saveAll(permissionsToAdd);
    }

    private void createUser(User user) {
        User newUser = new User();
        newUser.setUserId(UUID.randomUUID());
        newUser.setUsername(user.getUsername());
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        newUser.setLastName(user.getLastName());
        newUser.setFirstName(user.getFirstName());
        newUser.setFirstLogin(0);
        newUser.setEnabled(true);
        newUser.setIsDeleted(false);
        for (Role role : user.getRoles()) {
            Optional<Role> roleOptional = roleRepository.findByRoleName(role.getRoleName());
            if (roleOptional.isPresent()) {
                newUser.getRoles().add(roleOptional.get());
            } else {
                throw new RuntimeException("Role not found: " + role.getRoleName());
            }
        }
        userRepository.save(newUser);
    }

    private void loadPortals(ObjectMapper mapper, JsonNode rootNode) throws JsonProcessingException {
        Portal[] portals = mapper.readValue(rootNode.get("portals").toString(), Portal[].class);
        for (Portal portal : portals) {
            Portal existingPortal = PortalRepository.findByPortalName(portal.getPortalName());
            if (existingPortal == null) {
                existingPortal = createPortal(portal);
            }

            for (Permission permission : portal.getPermissions()) {
                Optional<Permission> permissionOptional = permissionRepository.findByPermissionName(permission.getPermissionName());
                if (permissionOptional.isPresent()) {
                    addPermissionToPortal(existingPortal, permissionOptional);
                } else {
                    throw new RuntimeException("Permission not found: " + permission.getPermissionName());
                }
            }

            PortalRepository.save(existingPortal);
        }
    }

    private void addPermissionToPortal(Portal existingPortal, Optional<Permission> permissionOptional) {
        Permission existingPermission = permissionOptional.get();
        existingPortal.getPermissions().add(existingPermission);
        existingPermission.setPortal(existingPortal);
        permissionRepository.save(existingPermission);
    }

    private Portal createPortal(Portal portal) {
        Portal existingPortal;
        existingPortal = new Portal();
        existingPortal.setPortalId(UUID.randomUUID());
        existingPortal.setPortalName(portal.getPortalName());
        existingPortal.setPortalDescription(portal.getPortalDescription());

        existingPortal.setPermissions(new ArrayList<>());
        PortalRepository.save(existingPortal);
        return existingPortal;
    }

    private void loadPortalInstances(ObjectMapper mapper, JsonNode rootNode) throws JsonProcessingException {
        PortalInstance[] portalInstances = mapper.readValue(rootNode.get("PortalInstances").toString(), PortalInstance[].class);
        for (PortalInstance portalInstance : portalInstances) {
            Portal existingPortal = PortalRepository.findByPortalName(portalInstance.getPortal().getPortalName());
            Optional<PortalInstance> portalInstanceOptional = Optional.ofNullable(PortalInstanceRepository.findByInstanceName(portalInstance.getInstanceName()));

            if (existingPortal != null  && !portalInstanceOptional.isPresent()) {
                PortalInstance portalInstance1 = new PortalInstance();
                portalInstance1.setPortalInstanceId(UUID.randomUUID());
                portalInstance1.setInstanceUrl(portalInstance.getInstanceUrl());
                portalInstance1.setInstanceName(portalInstance.getInstanceName());
                portalInstance1.setPortal(existingPortal);
                portalInstanceRepository.save(portalInstance1);
            }
        }
    }
    public void assignPermissionToRole(String permissionName, UUID roleId) {
        Permission permission = permissionRepository.findByPermissionName(permissionName)
                .orElseThrow(() -> new IllegalArgumentException("Permission not found"));

        Role role = roleRepository.findById(roleId)
                .orElseThrow(() -> new IllegalArgumentException("Role not found"));
        role.getPermissions().add(permission);
        permission.getRoles().add(role);
        roleRepository.save(role);
        permissionRepository.save(permission);
    }
}
