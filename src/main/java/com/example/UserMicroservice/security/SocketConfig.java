package com.example.UserMicroservice.security;


import com.corundumstudio.socketio.SocketIOServer;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;
@Configuration
public class SocketConfig {

    @Bean
    public SocketIOServer socketIOServer() {
        com.corundumstudio.socketio.Configuration config = new com.corundumstudio.socketio.Configuration();
        config.setHostname("0.0.0.0"); // Bind to all available IP addresses
        config.setPort(8083);
        return new SocketIOServer(config);
    }
}
