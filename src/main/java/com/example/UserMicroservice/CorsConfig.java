package com.example.UserMicroservice;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig implements WebMvcConfigurer {
//frontend et backend sont hébergés sur des domaines différents
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry
            .addMapping("/**")// Appliquer CORS à toutes les routesps aux | grep sonarqube

            .allowedOrigins("http://localhost:3000", "http://localhost:3001", "http://localhost:3002")
            .allowedMethods("GET", "POST", "PUT", "DELETE","PATCH", "OPTIONS")
            .allowedHeaders("*")
            .allowCredentials(true)
            .maxAge(3600);
    }
}
