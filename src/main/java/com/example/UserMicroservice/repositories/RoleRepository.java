package com.example.UserMicroservice.repositories;

import com.example.UserMicroservice.entities.Portal;
import com.example.UserMicroservice.entities.Role;
import com.example.UserMicroservice.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<Role, UUID> {
    Optional<Role> findByRoleName(String roleName);
    List<Role> findByIsDeletedFalse();
}
