package com.example.UserMicroservice.repositories;

import com.example.UserMicroservice.entities.Permission;
import com.example.UserMicroservice.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, UUID> {
    Optional<Permission> findByPermissionName(String permissionName);
//    List<Permission> findByRoles_Id(UUID roleId);
}
