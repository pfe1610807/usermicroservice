package com.example.UserMicroservice.repositories;

import com.example.UserMicroservice.entities.Portal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PortalRepository extends JpaRepository<Portal, UUID> {



    List<Portal> findByIsDeletedFalse();

    Portal findByPortalName(String portalName);
}
