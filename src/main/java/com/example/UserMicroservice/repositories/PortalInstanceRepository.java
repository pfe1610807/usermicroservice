package com.example.UserMicroservice.repositories;

import com.example.UserMicroservice.entities.Portal;
import com.example.UserMicroservice.entities.PortalInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository

public interface PortalInstanceRepository extends JpaRepository<PortalInstance, UUID> {
    PortalInstance findByInstanceName(String instanceName);
    // Optional<PortalInstance> findByPortalInstanceId(UUID portalInstanceId);
    List<PortalInstance> findByIsDeletedFalse();
    // List<PortalInstance> findByPortal_PortalId(UUID portalId);
    List<PortalInstance> findByPortal(Portal portal);
}
