package com.example.UserMicroservice.repositories;

import com.example.UserMicroservice.entities.Notification;
import com.example.UserMicroservice.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface NotificationRepository extends JpaRepository<Notification, UUID> {
    List<Notification> findByUser(User user);
    List<Notification> findByUserUserId(UUID userId);

}
