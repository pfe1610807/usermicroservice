package com.example.UserMicroservice.jwt;


import java.io.IOException;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class UnAuthorizedUserAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(jakarta.servlet.http.HttpServletRequest request,
                         jakarta.servlet.http.HttpServletResponse response,
                         AuthenticationException authException)
            throws IOException, jakarta.servlet.ServletException {

        response.sendError(HttpServletResponse.SC_UNAUTHORIZED,"UnAuthorized User");
    }


}
