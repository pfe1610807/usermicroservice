package com.example.UserMicroservice;

import com.corundumstudio.socketio.SocketIOServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.sql.init.SqlInitializationAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableJpaAuditing

public class UserMicroserviceApplication implements CommandLineRunner {
    private final SocketIOServer socketIOServer;
    @Autowired
    public UserMicroserviceApplication(SocketIOServer socketIOServer) {
        this.socketIOServer = socketIOServer;
    }

	public static void main(String[] args) {
		SpringApplication.run(UserMicroserviceApplication.class, args);
	}
    @Override
    public void run(String... args) throws Exception {
        socketIOServer.start();
        Runtime.getRuntime().addShutdownHook(new Thread(socketIOServer::stop));
    }
}
