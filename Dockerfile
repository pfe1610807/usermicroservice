# Use the OpenJDK 17 image from Docker Hub
FROM openjdk:17

# Expose the port on which your Spring Boot application is listening (make sure to use the correct port)
EXPOSE 8081

# Copy the JAR file from the project's target directory to the Docker image
ADD target/UserMicroservice-3.2.3.jar UserMicroservice-3.2.3.jar

# Command to run the Spring Boot application
ENTRYPOINT ["java", "-jar", "/UserMicroservice-3.2.3.jar"]
